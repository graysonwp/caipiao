#!/usr/bin/python3  

# -*- coding: utf-8 -*-
# @Author   : Grayson
# @Time     : 2021-03-05 10:20
# @Email    : weipengweibeibei@163.com
# @Description  : 常量类

# 数据库名称
DATABASE_NAME = 'caipiao'

# 接口地址
# 双色球开奖信息接口地址
TWO_COLOR_BALL_API_URL = 'http://datachart.500.com/ssq/history/newinc/history.php'

# 彩票数量
CAIPIAO_NUM = 4

# 号码分隔符
NUM_SEPERATOR = ','